﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using OrderSystem.Data.Model;

namespace OrderSystem.Data.Store
{
    public class OrderStore
    {
        public OrderDbContext DbContext { get; }

        public OrderStore(OrderDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IEnumerable<Order> GetOrders() =>
            DbContext.Orders.Include(o => o.Customer).OrderBy(o => o.OrderDate);
        public IEnumerable<Order> GetOrders(long customerId) =>
            DbContext.Orders.Where(o => o.CustomerId == customerId).OrderBy(o => o.OrderDate);
        public Order Get(long id) => DbContext.Orders
            .Include(o => o.Customer)
            .Include(o => o.Lines).ThenInclude(l => l.Product)
            .Single(c => c.Id == id);

        public void Add(Order order)
        {
            DbContext.Orders.Add(order);
            DbContext.SaveChanges();
        }

        public void Delete(long id)
        {
            DbContext.Orders.Remove(Get(id));
            DbContext.SaveChanges();
        }

        public void Update(Order order)
        {
            DbContext.Orders.Attach(order);
            DbContext.SaveChanges();
        }
    }
}
