﻿using System;
using OrderSystem.Data.Model;
using Microsoft.EntityFrameworkCore;

namespace OrderSystem.Data
{
    public class OrderDbContext : DbContext
    {
        protected OrderDbContext()
        {
        }

        public OrderDbContext(DbContextOptions options) : base(options)
        {
        }

        public DbSet<Customer> Customers { get; set; }
        public DbSet<Product> Products { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderLine> OrderLines { get; set; }

    }
}
