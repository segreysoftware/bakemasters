﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Data.Model
{
    public class Customer
    {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
