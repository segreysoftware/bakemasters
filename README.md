BakeMasters Exercise
====================

Introduction
------------
Thank you for you time and effort in completing this exercise. Let's start with some context
and instructions on how to complete the exercise.

This project is a small sample application. Further down in this document we describe a request
for adding a new functionality to this application. The exercise consists of implementing this
change.

Some rules and clarifications:

*  This is not an exam. There are not correct or incorrect solutions. We just want a piece of
   code you wrote as a starting-point for a discussion.
*  Keep it simple. We do not expect you to spend days developing an enterprise-grade, super
   robust implementation. A simple and to-the-point implementation that works correctly and
   efficiently is all we are after. This way the exercise should only consume a couple of hours
   of your time.
*  We do not make any claims towards the quality of design and implementation the sample
   application.
*  You are free to refactor the existing codebase any way you like, as long as the
   functionality of the application remains the same. Note that refactoring the codebase is not
   the goal of the exercise, so you are certainly not required or expected to do so.
*  The sample application is built using .net 5 and Blazor. But don't worry, you don't need to
   know anything about Blazor to complete the exercise.
*  You are free to use any open-source nuget-packages in your implementation.
*  Your solutions should not require us to install any additional software in order to run it.
*  If you have any questions while working on the exercise, you can mail them to
   bakemaster@segrey.com. We will try to answer quickly.

Completing the exercise
-----------------------
To complete the exercise you should:

*  Check out the sample project and build it on your machine.
*  Create a branch on which you will commit your changes
*  Read the instructions below and implement the requested functionality.
*  Commit your changes on the branch
*  Zip your repository and send it by email to bakemaster@segrey.com

Building the sample application
-------------------------------
You will need to have the following freely availabe software installed on your machine:

*  .net 5 SDK (or higher). https://dotnet.microsoft.com/download
*  Visual Studio 2019 (hor higher), any edition. The community edition can be downloaded for free:
   https://visualstudio.microsoft.com/downloads/

To minimize the prerquisits for running the application, the application uses the in memory
database included in entity framework. If you prefer a databse in SQL server you are free to
change the code to do so. (We have SQL server too, so no problem there :) )

Functional background
---------------------
The ficticious company for this exercise is a bakery called "Bakemasters". Appart from their
retail outlet, this bakery also supplies their products to a number of companies. To track the
orders these companies place, they've developped a small system, which they simply named the
Order System. It allows registering orders placed by customers and following up the
fulfillment status.

Change Request
--------------
One of the customers, The Olive Tree, is requesting the option to send orders through a
file-based interface, to improve efficiency. They have provided a sample order file to
illustrate what the file format would look like. This file can be found in the Files directory.
Your job is to extend the application so that it can load files in the format requested and
create the corresponding orders in the system.


