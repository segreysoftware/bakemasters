﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OrderSystem.Data.Model
{
    public class Order
    {
        public long Id { get; set; }
        public DateTime OrderDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Notes { get; set; }
        public long CustomerId { get; set; }
        public Customer Customer { get; set; }
        public List<OrderLine> Lines { get; set; }
        public string Status { get; set; }
    }
}
