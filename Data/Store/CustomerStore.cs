﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSystem.Data.Model;

namespace OrderSystem.Data.Store
{
    public class CustomerStore
    {
        public OrderDbContext DbContext { get; }

        public CustomerStore(OrderDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IEnumerable<Customer> Get() => DbContext.Customers.OrderBy(c => c.Name);
        public Customer Get(long id) => DbContext.Customers.Single(c => c.Id == id);

        public void Add(Customer customer)
        {
            DbContext.Customers.Add(customer);
            DbContext.SaveChanges();
        }

        public void Delete(long id)
        {
            DbContext.Customers.Remove(Get(id));
            DbContext.SaveChanges();
        }

        public void Update(Customer customer)
        {
            DbContext.Attach(customer);
            DbContext.SaveChanges();
        }
    }
}
