﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSystem.Data.Model;

namespace OrderSystem.Data
{
    public static class DatabasePopulator
    {
        public static void PopulateDatabase(this OrderDbContext db)
        {
            foreach (var name in new[]
                {"The Jackson Group", "The Olive Tree", "Toms Catering", "Toni", "Travis Mendoza", "Willie Burke"})
            {
                db.Customers.Add(new Customer() { Name = name });
            }

            db.Products.Add(new Product() { Name = "White Bread", Active = true});
            db.Products.Add(new Product() { Name = "Brown Bread", Active = true });
            db.Products.Add(new Product() { Name = "Doughnut", Active = true });
            db.Products.Add(new Product() { Name = "Baguette", Active = true });
            db.Products.Add(new Product() { Name = "Corn Bread", Active = true });
            db.Products.Add(new Product() { Name = "Waffle", Active = false });

            db.Orders.Add(
                new Order { CustomerId = 2, DeliveryDate = DateTime.Now.AddDays(5), OrderDate = DateTime.Now, Status = "new"});
            db.Orders.Add(
                new Order { CustomerId = 3, DeliveryDate = DateTime.Now.AddDays(2), OrderDate = DateTime.Now.AddDays(-1), Status = "new" });
            db.Orders.Add(
                new Order { CustomerId = 4, DeliveryDate = DateTime.Now.AddDays(1), OrderDate = DateTime.Now.AddDays(-3), Status = "preparing" });
            db.Orders.Add(
                new Order { CustomerId = 5, DeliveryDate = DateTime.Now.AddDays(3), OrderDate = DateTime.Now.AddDays(-2), Status = "shipped" });

            db.OrderLines.Add(new OrderLine() { OrderId = 1, ProductId = 2, Quantity = 1 });
            db.OrderLines.Add(new OrderLine() { OrderId = 2, ProductId = 1, Quantity = 1 });
            db.OrderLines.Add(new OrderLine() { OrderId = 2, ProductId = 4, Quantity = 1 });
            db.OrderLines.Add(new OrderLine() { OrderId = 3, ProductId = 3, Quantity = 6 });
            db.OrderLines.Add(new OrderLine() { OrderId = 3, ProductId = 2, Quantity = 5 });
            db.OrderLines.Add(new OrderLine() { OrderId = 3, ProductId = 5, Quantity = 1 });
            db.OrderLines.Add(new OrderLine() { OrderId = 4, ProductId = 1, Quantity = 3 });
            db.OrderLines.Add(new OrderLine() { OrderId = 4, ProductId = 5, Quantity = 1 });
            db.SaveChanges();
        }
        
    }
}

