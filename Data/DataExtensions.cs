﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSystem.Data.Model;
using OrderSystem.Data.Store;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace OrderSystem.Data
{
    public static class DataExtensions
    {
        public static void AddData(this IServiceCollection services)
        {
            services.AddDbContext<OrderDbContext>(options => options.UseInMemoryDatabase("Orders"));
            services.AddScoped<CustomerStore>();
            services.AddScoped<ProductStore>();
            services.AddScoped<OrderStore>();
        }
    }
}
