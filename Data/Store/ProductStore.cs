﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrderSystem.Data.Model;

namespace OrderSystem.Data.Store
{
    public class ProductStore
    {
        public OrderDbContext DbContext { get; }

        public ProductStore(OrderDbContext dbContext)
        {
            DbContext = dbContext;
        }

        public IEnumerable<Product> Get() => DbContext.Products.OrderBy(p => p.Name);
        public Product Get(long id) => DbContext.Products.Single(c => c.Id == id);

        public void Add(Product product)
        {
            DbContext.Products.Add(product);
            DbContext.SaveChanges();
        }

        public void Delete(long id)
        {
            DbContext.Products.Remove(Get(id));
            DbContext.SaveChanges();
        }

        public void Update(Product product)
        {
            DbContext.Attach(product);
            DbContext.SaveChanges();
        }
    }
}
